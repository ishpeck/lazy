package lazy

import (
	"testing"
	"time"
)

func TestChainMapNonFunc(tst *testing.T) {
	ch := Chain(0).Map(0)
	if ch.Ok() || ch.Complaint().Error() != "Type issue: Map() must receive a function as its argument." {
		tst.Errorf("Unexpected result from Map(0): %v", ch.Complaint())
	}
}

func checkBadArity(tst *testing.T, ch Chainable) {
	if ch.Ok() {
		tst.Errorf("Arity wasn't enforced!")
	}
	if ch.Complaint().Error() != "Arity issue: Map() must have a monadic function." {
		tst.Errorf("Arity enforcement error expected: %v", ch.Complaint())
	}
}

func TestChainMapBadArity(tst *testing.T) {
	checkBadArity(tst, Chain().Map(func() {}))
	checkBadArity(tst, Chain().Map(func(x, y int) {}))
	checkBadArity(tst, Chain().Map(func(x, y, z int) {}))
}

func TestChainMapBadReturnSig(tst *testing.T) {
	stencil := func(x int) (int, error) { return 0, nil }
	expected := "Return signature issue: Map() must return 1 value."
	err := Chain().Map(stencil).Complaint().Error()
	if err != expected {
		tst.Errorf("Retsig enforcement unexpected: %v", err)
	}
}

func TestChainMap(tst *testing.T) {
	ch := Chain(1, 2, 3, 4).Map(func(x int) int { return x + 1 })
	if !ch.Ok() {
		tst.Errorf("Map failure: Map output wasn't OK: %v", ch.Complaint())
	}
	val := ch.First()
	if val != 2 {
		tst.Errorf("Map() didn't run the incrementor as expected: %v (%T)", val, val)
	}
	all := ch.Every()
	for idx, val := range all {
		expect := idx + 2
		if val != expect {
			tst.Errorf("Map()[%d] != %d but %d instead! ", idx, expect, val)
		}
	}
}

func TestChainMapBadInputs(tst *testing.T) {
	ch := Chain("a", 2, 3, 4).Map(func(x int) int { return x + 1 })
	val := ch.First()
	if val.(error).Error() != "a is the wrong type for mapped function" {
		tst.Errorf("Map() didn't run the incrementor as expected: %v (%T)", val, val)
	}
}

func TestChainMapLaziness(tst *testing.T) {
	callCount := 0
	fn := func(x int) int {
		callCount += 1
		return callCount
	}
	ch := Chain(0).Map(fn)
	if callCount != 0 {
		tst.Errorf("Map(fn) shouldn't have called fn yet.")
	}
	out := ch.Map(fn).First()
	if out != 2 {
		tst.Errorf("Map(fn) should have called fn twice at resolution time.")
	}
	if callCount != 2 {
		tst.Errorf("callCount state")
	}
}

func TestChainMapOnBrokenChain(tst *testing.T) {
	ch := Break("Deliberate failure").Map(func(x int) int { return x + 1 })
	if ch.Ok() {
		tst.Errorf("Expecting breakage, not map happies")
	}
	if ch.Complaint().Error() != "Deliberate failure" {
		tst.Errorf("Break.Map() is not an identity function")
	}
	out := ch.First()
	if out != nil {
		tst.Errorf("Break.Map(): Expected <nil> but got %v", out)
	}
}

func perfTestParallelMapResolution(bnch *testing.B) {
	fn := func(x int) int {
		time.Sleep(time.Second * 1)
		return x - 1
	}
	Chain(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).Map(fn).Every()
}

func TestChainParallelResolution(tst *testing.T) {
	res := testing.Benchmark(perfTestParallelMapResolution)
	dur := res.NsPerOp()
	patienceInNanoseconds := int64(2000000000)
	if dur > patienceInNanoseconds {
		tst.Errorf("Resolution too slow!  Execution time (in nanoseconds) is %d", dur)
	}
}

package lazy

import (
	"fmt"
	"testing"
)

func TestGuardOnBrokenChain(tst *testing.T) {
	ch := Break("Deliberate failure")
	gd := ch.Guard(nil)
	if gd != ch {
		tst.Errorf("Unexpected Guard() failure: %s", gd)
	}
}

func TestGuardBads(tst *testing.T) {
	errMsg := "TWO IS EVIL!"
	if Chain().Guard(nil).Ok() {
		tst.Errorf("Guard(nil) shouldn't be OK")
	}
	ch := Chain(1, 2).Guard(func(x int) error {
		if x == 2 {
			return fmt.Errorf(errMsg)
		}
		return nil
	})
	if ch.String() != errMsg {
		tst.Errorf("Unexpected error from Guard(): %s", ch)
	}
	killed := Chain().Guard(func() {})
	if killed.Ok() {
		tst.Errorf("Guard() should reject a non-returning procedure: %s", killed)
	}
	rekilled := Chain().Guard(func() (int, int) { return 0, 0 })
	if rekilled.Ok() {
		tst.Errorf("Guard() should reject a non-returning procedure: %s", rekilled)
	}
}

func TestGuardGoods(tst *testing.T) {
	callCount := 0
	expecting := []interface{}{1, 2, 3}
	ch := Chain(expecting...)
	gd := ch.Guard(func(x int) error {
		callCount += 1
		return nil
	})
	if ch == gd {
		tst.Errorf("Guard() shouldn't be an identity function for performance reasons.")
		tst.FailNow()
	}
	out := gd.Every()
	if callCount != len(expecting) {
		tst.Errorf("Expected Guard()'s fn to get called once per element.")
	}
	if len(out) != len(expecting) {
		tst.Errorf("Expecting Guard()'s output to be the same as its input chain")
		tst.FailNow()
	}
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("Guard()[%d] != %d but %d instead.", idx, expect, out[idx])
		}
	}
}

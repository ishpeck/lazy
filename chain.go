package lazy

import (
	"reflect"
	"sync"
)

type StencilFn interface{}

type Chainable interface {
	After(int) Chainable
	Apply(StencilFn) Chainable
	Complaint() error
	Collapse() Chainable
	Every() []interface{}
	Expand(...int) Chainable
	Filter(StencilFn) Chainable
	First() interface{}
	Fold(StencilFn) Chainable
	Guard(StencilFn) Chainable
	Map(StencilFn) Chainable
	Ok() bool
	Pleat(StencilFn) Chainable
	Resolve() ([]interface{}, error)
	String() string
	Take(int) Chainable
}

var (
	APPLY_FAILURE_NON_FUNCTION     = Break("Type issue: Apply() must receive a function.")
	APPLY_FAILURE_INSUFFICIENT_DAT = Break("Artiy issue: Apply(fn) requires the chain to contain enough data to pass in to fn.")
	APPLY_FAILURE_EXCESSIVE_DAT    = Break("Artiy issue: Apply(fn) requires the chain to contain exactly as many elements as fn's arguments count.")
	FILTER_FAILURE_NON_FUNCTION    = Break("Type issue: Filter() must receive a function.")
	FILTER_FAILURE_WRONG_RETSIG    = Break("Return signature issue: Filter()'s function must be a predicate (returning single bool)")
	FILTER_FAILURE_NON_MONADIC     = Break("Arity issue: Filter() must be a monadic function.")
	FOLD_FAILURE_NON_DYADIC        = Break("Arity issue: Fold() requires a dyadic function.")
	FOLD_FAILURE_NON_FUNCTION      = Break("Type issue: Fold() requires a function as its argument")
	FOLD_FAILURE_NON_SINGLE_RETURN = Break("Return signature issue: Fold() requires a function that returns just 1 value")
	MAP_FAILURE_NON_FUNCTION       = Break("Type issue: Map() must receive a function as its argument.")
	MAP_FAILURE_NON_MONADIC        = Break("Arity issue: Map() must have a monadic function.")
	MAP_FAILURE_NON_SINGLE_RETURN  = Break("Return signature issue: Map() must return 1 value.")
	PLEAT_FAILURE_NON_FUNCTION     = Break("Type issue: Pleat() requires a function")
	PLEAT_FAILURE_NON_DYADIC       = Break("Arity issue: Pleat() requires a dyadic function")
	PLEAT_FAILURE_NON_1_RETURN     = Break("Return signature issue: Pleat() requires a function with single return value")
)

type chLink struct {
	dat Sequence
	num int
}

func (cl *chLink) next(newDat Sequence) Chainable {
	return &chLink{
		newDat,
		cl.num + 1,
	}
}

func (cl *chLink) nth(idx int) Getter {
	cur := -1
	for cl.dat.Reset(); cl.dat.Valid(); cl.dat.Advance() {
		cur += 1
		if cur == idx {
			return cl.dat.Cur()
		}
	}
	return nil
}

func (cl *chLink) After(idx int) Chainable {
	return cl.next(SpawnSeq(cl.dat.Sub(idx, cl.dat.Len())...))
}

func (cl *chLink) Apply(fn StencilFn) Chainable {
	fnVal := reflect.ValueOf(fn)
	if fnVal.Kind() != reflect.Func {
		return APPLY_FAILURE_NON_FUNCTION
	}
	fnTyp := fnVal.Type()
	if fnTyp.NumIn() > cl.dat.Len() {
		return APPLY_FAILURE_INSUFFICIENT_DAT
	}
	if fnTyp.NumIn() < cl.dat.Len() {
		return APPLY_FAILURE_EXCESSIVE_DAT
	}
	return cl.next(SpawnUncachedSeq(func() []Getter {
		var out []Getter
		var ins []reflect.Value
		for cl.dat.Reset(); cl.dat.Valid(); cl.dat.Advance() {
			get := cl.dat.Cur()
			ins = append(ins, reflect.ValueOf(get()))
		}
		for _, ov := range fnVal.Call(ins) {
			out = append(out, Gen(ov.Interface()))
		}
		return out
	}))
}

func (cl *chLink) Every() []interface{} {
	output := make([]interface{}, cl.dat.Len())
	wg := &sync.WaitGroup{}
	max := cl.dat.Len()
	cl.dat.Reset()
	for idx := 0; idx < max && cl.dat.Valid(); idx += 1 {
		getter := cl.dat.Cur()
		wg.Add(1)
		go func(i int, get Getter) {
			output[i] = get()
			wg.Done()
		}(idx, getter)
		cl.dat.Advance()
	}
	wg.Wait()
	return output
}

func (cl *chLink) Expand(idxs ...int) Chainable {
	var retMe []interface{}
	for _, idx := range idxs {
		getter := cl.nth(idx)
		if getter == nil {
			continue
		}
		maybeSlice := reflect.ValueOf(getter())
		if maybeSlice.Kind() != reflect.Slice {
			retMe = append(retMe, maybeSlice.Interface())
			continue
		}
		for idx := 0; idx < maybeSlice.Len(); idx += 1 {
			retMe = append(retMe, maybeSlice.Index(idx).Interface())
		}
	}
	return cl.next(Seq(retMe...))
}

func (cl *chLink) Filter(fn StencilFn) Chainable {
	fnVal := reflect.ValueOf(fn)
	if fnVal.Kind() != reflect.Func {
		return FILTER_FAILURE_NON_FUNCTION
	}
	fnTyp := fnVal.Type()
	if fnTyp.NumIn() != 1 {
		return FILTER_FAILURE_NON_MONADIC
	}
	if fnTyp.NumOut() != 1 {
		return FILTER_FAILURE_WRONG_RETSIG
	}
	return cl.next(SpawnUncachedSeq(func() []Getter {
		var out []Getter
		for cl.dat.Reset(); cl.dat.Valid(); cl.dat.Advance() {
			gotten := cl.dat.Cur()()
			result := fnVal.Call([]reflect.Value{
				reflect.ValueOf(gotten),
			})
			if len(result) == 1 && result[0].Bool() {
				out = append(out, Gen(gotten))
			}
		}
		return out
	}))
}

func (cl *chLink) First() interface{} {
	cl.dat.Reset()
	if cl.dat.Valid() {
		return cl.dat.Cur()()
	}
	return nil
}

func (cl *chLink) Fold(fn StencilFn) Chainable {
	fnVal := reflect.ValueOf(fn)
	if fnVal.Kind() != reflect.Func {
		return FOLD_FAILURE_NON_FUNCTION
	}
	fnTyp := fnVal.Type()
	if fnTyp.NumIn() != 2 {
		return FOLD_FAILURE_NON_DYADIC
	}
	if fnTyp.NumOut() != 1 {
		return FOLD_FAILURE_NON_SINGLE_RETURN
	}
	return cl.next(SpawnSeq(func() interface{} {
		cl.dat.Reset()
		hold := reflect.ValueOf(cl.dat.Cur()())
		cl.dat.Advance()
		for cl.dat.Valid(); cl.dat.Valid(); cl.dat.Advance() {
			out := fnVal.Call([]reflect.Value{
				hold,
				reflect.ValueOf(cl.dat.Cur()()),
			})
			hold = out[0]
		}
		return hold.Interface()
	}))
}

func (cl *chLink) Ok() bool {
	return true
}

func pleatify(fn reflect.Value, left, right Getter) Getter {
	return func() interface{} {
		larg := left()
		rarg := right()
		results := fn.Call([]reflect.Value{
			reflect.ValueOf(larg),
			reflect.ValueOf(rarg),
		})
		return results[0].Interface()
	}
}

func (cl *chLink) Pleat(fn StencilFn) Chainable {
	var out []Getter
	fnVal := reflect.ValueOf(fn)
	if fnVal.Kind() != reflect.Func {
		return PLEAT_FAILURE_NON_FUNCTION
	}
	fnTyp := fnVal.Type()
	if fnTyp.NumIn() != 2 {
		return PLEAT_FAILURE_NON_DYADIC
	}
	if fnTyp.NumOut() != 1 {
		return PLEAT_FAILURE_NON_1_RETURN
	}
	cl.dat.Reset()
	left := cl.dat.Cur()
	for cl.dat.Advance(); cl.dat.Valid(); cl.dat.Advance() {
		right := cl.dat.Cur()
		out = append(out, pleatify(fnVal, left, right))
		left = right
	}
	return cl.next(SpawnSeq(out...))
}

func (cl *chLink) Resolve() ([]interface{}, error) {
	return cl.Every(), cl.Complaint()
}

func (cl *chLink) Collapse() Chainable {
	return cl.next(SpawnSeq(func() interface{} {
		var out []interface{}
		for cl.dat.Reset(); cl.dat.Valid(); cl.dat.Advance() {
			out = append(out, cl.dat.Cur()())
		}
		return out
	}))
}

func (cl *chLink) Complaint() error {
	return nil
}

func (cl *chLink) Guard(fn StencilFn) Chainable {
	var cache []interface{}
	fnVal := reflect.ValueOf(fn)
	if fnVal.Kind() != reflect.Func {
		return Break("Guard() needs a function")
	}
	fnTyp := fnVal.Type()
	if fnTyp.NumOut() != 1 {
		return Break("Guard(fn): fn() doesn't return exactly 1 error")
	}
	for cl.dat.Reset(); cl.dat.Valid(); cl.dat.Advance() {
		curVal := cl.dat.Cur()()
		out := fnVal.Call([]reflect.Value{
			reflect.ValueOf(curVal),
		})
		err := out[0].Interface()
		if err != nil {
			return Break("%v", err)
		}
		cache = append(cache, curVal)
	}
	return Chain(cache...)
}

func mapify(fn reflect.Value, tp reflect.Type, get Getter) Getter {
	return func() interface{} {
		passIn := get()
		if reflect.TypeOf(passIn) != tp.In(0) {
			return Break("%v is the wrong type for mapped function", passIn).Complaint()
		}
		out := fn.Call([]reflect.Value{reflect.ValueOf(passIn)})
		return out[0].Interface()
	}
}

func (cl *chLink) Map(fn StencilFn) Chainable {
	fnVal := reflect.ValueOf(fn)
	if fnVal.Kind() != reflect.Func {
		return MAP_FAILURE_NON_FUNCTION
	}
	fnTyp := reflect.TypeOf(fn)
	if fnTyp.NumIn() != 1 {
		return MAP_FAILURE_NON_MONADIC
	}
	if fnTyp.NumOut() != 1 {
		return MAP_FAILURE_NON_SINGLE_RETURN
	}
	return cl.next(SpawnUncachedSeq(func() []Getter {
		var out []Getter
		for cl.dat.Reset(); cl.dat.Valid(); cl.dat.Advance() {
			cur := cl.dat.Cur()
			out = append(out, mapify(fnVal, fnTyp, cur))
		}
		return out
	}))
}

func (cl *chLink) String() string {
	return Break("<chain step %d>", cl.num).String()
}

func (cl *chLink) Take(idx int) Chainable {
	return cl.next(SpawnSeq(cl.dat.Sub(idx)...))
}

func SpawnChainLink(args ...interface{}) *chLink {
	return &chLink{
		Seq(args...),
		0,
	}
}

func Chain(args ...interface{}) Chainable {
	return SpawnChainLink(args...)
}

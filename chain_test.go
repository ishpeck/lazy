package lazy

import (
	"testing"
)

func checkChain(tst *testing.T, ch Chainable, firstThing interface{}) {
	if !ch.Ok() {
		tst.Errorf("Chains should be OK unless they're broken!")
	}
	if ch.Complaint() != nil {
		tst.Errorf("Chains should never have complaints unless they're broken!")
	}
	gotten := ch.First()
	if gotten != firstThing {
		tst.Errorf("Chain.First() expecting %v but got %v instead.", firstThing, gotten)
	}
	ch.String()
	_, err := ch.Resolve()
	if err != nil {
		tst.Errorf("Good chains shouldn't resolve to errors.")
	}
}

func checkBroken(tst *testing.T, ch Chainable, expect string) {
	if ch.Ok() {
		tst.Errorf("Broken chains should never be OK")
	}
	if ch.Complaint() == nil {
		tst.Errorf("Broken chains should have complaints!")
	}
	if ch.Complaint().Error() != expect {
		tst.Errorf("Unexpected error message: %v", ch.Complaint())
	}
	if ch.String() != expect {
		tst.Errorf("Unexpected strigification: %v", ch.Complaint())
	}
	gotten := ch.First()
	if gotten != nil {
		tst.Errorf("Break.First() expecting nil but got %v", gotten)
	}
	many := ch.Every()
	if many != nil {
		tst.Errorf("Break.Every() expecting nil but got %v", many)
	}
	_, err := ch.Resolve()
	if err.Error() != expect {
		tst.Errorf("Break.Resolve() unexpected error: %s", ch)
	}
}

func TestChainType(tst *testing.T) {
	checkChain(tst, Chain(), nil)
	checkChain(tst, Chain("a"), "a")
	msg := "Deliberate failure for testing"
	checkBroken(tst, Break(msg), msg)
}

func TestBigFatLazyChain(tst *testing.T) {
	add1Called := 0
	evnsCalled := 0
	tenxCalled := 0
	add1 := func(x int) int {
		add1Called += 1
		return x + 1
	}
	evns := func(x int) bool {
		evnsCalled += 1
		return x%2 == 0
	}
	tenx := func(x int) int {
		tenxCalled += 1
		return x * 10
	}
	ch := Chain(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).Map(add1).Filter(evns).Map(tenx)
	expecting := []int{20, 40, 60, 80, 100}
	if !ch.Ok() {
		tst.Errorf("Big fat Unexpected chain failure: %s", ch)
	}
	if add1Called != 0 {
		tst.Errorf("Big fat Laziness failed for add1Called!  Called %d times!", add1Called)
	}
	if evnsCalled != 0 {
		tst.Errorf("Big fat Laziness failed for evnsCalled!  Called %d times!", evnsCalled)
	}
	if tenxCalled != 0 {
		tst.Errorf("Big fat Laziness failed for tenxCalled!  Called %d times!", tenxCalled)
	}
	out := ch.Every()
	if len(out) != len(expecting) {
		tst.Errorf("Big fat Wrong number of returned things: %d", len(out))
	}
	for idx, expect := range expecting {
		if expect != out[idx] {
			tst.Errorf("Big fat Expected %d but got %d.", expect, out[idx])
		}
	}
}

package lazy

import (
	"testing"
)

func TestPleatOnBrokenChain(tst *testing.T) {
	ch := Break("Deliberate failure.")
	pl := ch.Pleat(nil)
	if pl != ch {
		tst.Errorf("Expected failure but got %s instead.", ch)
	}
}

func TestPleatStep1(tst *testing.T) {
	expecting := []int{3, 5, 7, 9}
	ch := Chain(1, 2, 3, 4, 5)
	pl := ch.Pleat(func(x, y int) int {
		return x + y
	})
	if !pl.Ok() {
		tst.Errorf("%s.Pleat() wasn't OK: %s", ch, pl)
		tst.FailNow()
	}
	out := pl.Every()
	if len(out) != len(expecting) {
		tst.Errorf("Pleat should've given us %d items but got %v", len(expecting), out)
	}
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("%s.Pleat()[%d] != %d but %d instead", ch, idx, expect, out[idx])
		}
	}
}

func TestPleatWithNonFunc(tst *testing.T) {
	ch := Chain(1, 2)
	plNil := ch.Pleat(nil)
	if plNil.Ok() {
		tst.Errorf("%s.Pleat(nil) should fail!", ch)
	}
	plInt := ch.Pleat(1)
	if plInt.Ok() {
		tst.Errorf("%s.Pleat(1) should fail!", ch)
	}
}

func TestPleatWithBadFuncs(tst *testing.T) {
	ch := Chain(1, 2)
	pl1 := ch.Pleat(func(x int) int { return x + 1 })
	if pl1.Ok() {
		tst.Errorf("%s.Pleat((int)=>int) should fail!", ch)
	}
	pl2 := ch.Pleat(func(x, y, z int) int { return x + y + z })
	if pl2.Ok() {
		tst.Errorf("%s.Pleat((int, int, int)=>int) should fail!", ch)
	}
	pl3 := ch.Pleat(func(x, y int) (int, error) { return x + y, nil })
	if pl3.Ok() {
		tst.Errorf("%s.Pleat((int,int)=>(int,err)) should fail!", ch)
	}
}

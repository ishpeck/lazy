package lazy

import (
	"testing"
)

func foldTestSumUp(x, y int) int {
	return x + y
}

func TestFoldFailureOnNonFunc(tst *testing.T) {
	ch := Chain().Fold(0)
	if ch.Ok() {
		tst.Errorf("Fold(0) shouldn't be OK")
	}
	if ch != FOLD_FAILURE_NON_FUNCTION {
		tst.Errorf("Fold(0) expects \"%s\" but got \"%s\" instead.", FOLD_FAILURE_NON_FUNCTION, ch)
	}
}

func TestFoldFailureOnMonadicFunc(tst *testing.T) {
	ch := Chain().Fold(func(x int) int {
		return x
	})
	if ch.Ok() || ch != FOLD_FAILURE_NON_DYADIC {
		tst.Errorf("Fold(monadic) should fail with \"%v\" but got \"%v\" instead.", FOLD_FAILURE_NON_DYADIC.Complaint(), ch.Complaint())
	}
}

func TestFoldFailureOnTriadicFunc(tst *testing.T) {
	ch := Chain().Fold(func(x, y, z int) int {
		return x + y + z
	})
	if ch.Ok() || ch != FOLD_FAILURE_NON_DYADIC {
		tst.Errorf("Fold(triadic) should fail with \"%s\" but got \"%v\" instead.", FOLD_FAILURE_NON_DYADIC, ch)
	}
}

func TestFoldFailureOnMultipleReturns(tst *testing.T) {
	ch := Chain().Fold(func(x, y int) (int, error) {
		return 0, nil
	})
	if ch.Ok() || ch != FOLD_FAILURE_NON_SINGLE_RETURN {
		tst.Errorf("Fold(=>(int,error)) should faile with \"%s\" but got \"%s\" instead.", FOLD_FAILURE_NON_SINGLE_RETURN, ch)
	}
}

func TestFoldBasicSuccess(tst *testing.T) {
	ch := Chain(1, 2, 3).Fold(foldTestSumUp)
	if !ch.Ok() {
		tst.Errorf("Fold(sumUp) should succed but got: %s", ch)
	}
	out := ch.First()
	expecting := 6
	if out != expecting {
		tst.Errorf("Fold(sumUp) should be %d but got %v instead.", expecting, out)
	}
}

func TestFoldLaziness(tst *testing.T) {
	wasCalled := 0
	mult := func(x, y int) int {
		wasCalled += 1
		return x * y
	}
	ch := Chain(2, 2, 2, 2).Fold(mult)
	if wasCalled != 0 {
		tst.Errorf("Fold(mult) shouldn't call our function yet.")
	}
	out := ch.First()
	if wasCalled != 3 {
		tst.Errorf("mult() called %d times instead of 3", wasCalled)
	}
	if out != 16 {
		tst.Errorf("Fold(mult) should yield 16 but got %v instead.", out)
	}
}

func TestFoldOnBrokenChain(tst *testing.T) {
	msg := "deliberate failure for fold test"
	ch := Break(msg).Fold(foldTestSumUp)

	if ch == nil || ch.Ok() {
		tst.Errorf("Should fail with broken chain.")
	}
	if ch.Complaint().Error() != msg {
		tst.Errorf("Unexpected fold error: %v", ch.Complaint())
	}
}

package lazy

import (
	"testing"
)

func checkSequenceType(tst *testing.T, seq Sequence, initial interface{}, second interface{}, third interface{}, fourth interface{}) {
	var gotten interface{}
	gotten = seq.Cur()()
	if gotten != initial {
		tst.Errorf("Sequence.Cur() expected %v but got %v", initial, gotten)
	}
	didAdvance := seq.Advance()
	if didAdvance && second == nil {
		tst.Errorf("Sequence.Advance() worked but we don't expect more than one element!")
	}
	if !didAdvance && second != nil {
		tst.Errorf("Sequence.Advance() should work if we have two or more elements: %v, %v", initial, second)
	}
	if !didAdvance {
		return
	}
	gotten = seq.Cur()()
	if gotten != second {
		tst.Errorf("Sequence.Advance()->Cur() should yield %v but got %v instead", second, gotten)
	}
	if third == nil {
		return
	}
	if !seq.Advance() {
		tst.Errorf("Sequence.Advance() a second time should work")
	}
	gotten = seq.Cur()()
	if gotten != third {
		tst.Errorf("Sequence.Cur() on the third element should yield %v but got %v instead.", third, gotten)
	}
	if seq.Advance() {
		tst.Errorf("Sequenc.Advance() should fail as we've exhausted the sequence")
	}
	gotten = seq.Cur()()
	if gotten != nil {
		tst.Errorf("Sequenc.Cur() should return nil but got %v instead.", gotten)
	}
}

func TestSequenceType(tst *testing.T) {
	checkSequenceType(tst, Seq(), nil, nil, nil, nil)
	checkSequenceType(tst, Seq(1), 1, nil, nil, nil)
	checkSequenceType(tst, Seq(2, 3), 2, 3, nil, nil)
	checkSequenceType(tst, Seq(4, 5, 6), 4, 5, 6, nil)
}

func TestSequenceLaziness(tst *testing.T) {
	wasCalled := 0
	seq := SpawnUncachedSeq(func() []Getter {
		wasCalled += 1
		return []Getter{Gen(1), Gen(0)}
	})
	if wasCalled != 0 {
		tst.Errorf("Shouldn't have called the fetcher to reify the sequence yet!")
	}
	gotten := seq.Cur()()
	if wasCalled != 1 {
		tst.Errorf("Wascalled flag should be set by now!")
	}
	if gotten != 1 {
		tst.Errorf("SeqLaziness: Unexpected value: %v", gotten)
	}
	seq.Advance()
	gotten = seq.Cur()()
	if wasCalled != 1 {
		tst.Errorf("We sholudn't call the fetcher more than once!")
	}
	if gotten != 0 {
		tst.Errorf("Sequence cache corrupted!")
	}

}

func TestCopyingSequence(tst *testing.T) {
	wasCalled := 0
	seq1 := SpawnUncachedSeq(func() []Getter {
		wasCalled += 1
		return []Getter{Gen(1), Gen(0)}
	})
	if seq1 == nil {
		tst.Errorf("Shouldn't get back a nil sequence before copy.")
	}
	seq2 := seq1.Copy()
	if seq2 == nil {
		tst.Errorf("Shouldn't get back a nil sequence after copy.")
	}
	if wasCalled != 0 {
		tst.Errorf("Shouldn't have resolved the sequence into cache yet!")
	}
	seq1.Valid()
	if wasCalled != 1 {
		tst.Errorf("Cache should've happend once did %d times instead.", wasCalled)
	}
	seq2.Valid()
	gotten := seq2.Cur()()
	if wasCalled != 2 {
		tst.Errorf("Each call should have reified separately but called %d times instead.", wasCalled)
	}
	if gotten == nil {
		tst.Errorf("seq2.Cur() shouldn't yield a nothing")
	}
	seq3 := seq2.Copy()
	if seq3.Cur()() != seq2.Cur()() {
		tst.Errorf("seq2 doesn't match its copy seq3!")
	}
	if wasCalled != 2 {
		tst.Errorf("seq3 is a copy of an already reified sequence and shouldn't call the initializer again")
	}
}

func TestSequenceLengths(tst *testing.T) {
	larry := Seq()
	if larry.Len() != 0 {
		tst.Errorf("Expected a zero length but got %d instead.", larry.Len())
	}
	curly := Seq(1)
	if curly.Len() != 1 {
		tst.Errorf("Expected a single length but got %d instead.", curly.Len())
	}
	moe := Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
	if moe.Len() != 10 {
		tst.Errorf("Expected ten things but got %d instead.", moe.Len())
	}
}

func TestSequenceResets(tst *testing.T) {
	seq := Seq(1, 2, 3)
	first := seq.Cur()()
	seq.Advance()
	seq.Advance()
	third := seq.Cur()()
	if third != 3 {
		tst.Errorf("Expecting current thing to be 3 but got %d", third)
	}
	seq.Reset()
	recent := seq.Cur()()
	if recent != first {
		tst.Errorf("Expecting current thing to be %d but got %d", first, recent)
	}
}

func TestSequenceSubSet(tst *testing.T) {
	seq := Seq(0, 1, 2, 3, 4, 5, 6)
	if seq.Sub() != nil {
		tst.Errorf("You asked for nothing and should get it!")
	}
	for idx, get := range seq.Sub(3) {
		val := get()
		if idx != val {
			tst.Errorf("Sub1 expected %d but got %d", idx, val)
		}
	}
	var recent int
	for idx, get := range seq.Sub(2, 5) {
		val := get()
		want := idx + 2
		recent = val.(int)
		if want != val {
			tst.Errorf("Sub2 expected %d but got %d", want, val)
		}
	}
	if recent != 4 {
		tst.Errorf("Last thing should be %d but is %d", 4, recent)
	}
	out := seq.Sub(0, 10000)
	if len(out) != 7 {
		tst.Errorf("Getting more than we have shouldn't ruin anything.")
	}
	out = seq.Sub(-12938, 10000)
	if len(out) != 7 {
		tst.Errorf("Excessively wide boundaries shouldn't ruin anything.")
	}
}

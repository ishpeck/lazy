package lazy

import (
	"testing"
)

func addThree(x, y, z int) int {
	return x + y + z
}

func add2Pairs(a, b, c, d int) (int, int) {
	return a + b, c + d
}

func TestChainFailureOnBadType(tst *testing.T) {
	ch := Chain(1, 2, 3).Apply(4)
	if ch.Ok() {
		tst.Errorf("Apply(4) shouldn't be OK.")
		tst.FailNow()
	}
	if ch != APPLY_FAILURE_NON_FUNCTION {
		tst.Errorf("Apply(4) Unexpected failure: %v", ch)
	}
}

func TestChainFailureOnInsufficientArgs(tst *testing.T) {
	ch := Chain(1, 2).Apply(addThree)
	if ch.Ok() {
		tst.Errorf("(1, 2) -> addThree should not be OK")
	}
	if ch != APPLY_FAILURE_INSUFFICIENT_DAT {
		tst.Errorf("(1, 2) -> addThree unexpected failure: %s", ch)
	}
}

func TestChainFailureOnTooManyArgs(tst *testing.T) {
	ch := Chain(1, 2, 3, 4).Apply(addThree)
	if ch.Ok() {
		tst.Errorf("(1, 2, 3, 4) -> addThree should not be OK: %v", ch.Every())
	}
	if ch != APPLY_FAILURE_EXCESSIVE_DAT {
		tst.Errorf("(1, 2, 3, 4) -> addThree unexpected failure: %s", ch)
	}
}

func TestChainBasicApply(tst *testing.T) {
	ch := Chain(1, 2, 3).Apply(addThree)
	if !ch.Ok() {
		tst.Errorf("Apply() failure: %v", ch)
	}
	gotten := ch.First()
	if gotten != 6 {
		tst.Errorf("Apply() results incorrect: %v", gotten)
	}
}

func TestApplyOnBrokenChain(tst *testing.T) {
	ch := Break("Deliberate failure").Apply(addThree)
	if ch == nil || ch.String() != "Deliberate failure" {
		tst.Errorf("Break.Apply() should be identity.")
	}
}

func TestApplyWithMultiReturn(tst *testing.T) {
	ch := Chain(2, 2, 4, 4).Apply(add2Pairs)
	if !ch.Ok() {
		tst.Errorf("Expected Apply(add2Pairs) to be OK")
		tst.FailNow()
	}
	out := ch.Every()
	if out[0] != 4 {
		tst.Errorf("Apply(add2Pairs) failed to yield satisfactory first resuult")
	}
	if out[1] != 8 {
		tst.Errorf("Apply(add2pairs) failed to yield satisfactory second result")
	}
}

package lazy

type Getter func() interface{}

func Gen(val interface{}) Getter {
	return func() interface{} { return val }
}

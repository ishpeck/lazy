package lazy

import (
	"testing"
)

func TestBasicGetter(tst *testing.T) {
	get := Gen(3)
	gotten := get()
	if gotten != 3 {
		tst.Errorf("Unexpected value from getter: %v", gotten)
	}
}

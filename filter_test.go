package lazy

import (
	"testing"
)

func isEven(x int) bool {
	return x%2 == 0
}

func TestFilterOnBrokenChain(tst *testing.T) {
	ch := Break("Deliberate failure")
	ft := ch.Filter(isEven)
	if ft.Ok() {
		tst.Errorf("Filtering broken chain shouldn't be OK")
	}
	if ft != ch {
		tst.Errorf("Filtering broken chains should be identity operation")
	}
}

func TestFilterFailureOnBadFuncType(tst *testing.T) {
	ch := Chain(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).Filter(isEven)
	expecting := []int{2, 4, 6, 8, 10}
	if !ch.Ok() {
		tst.Errorf("Basic filtering expected to be OK.")
	}
	out := ch.Every()
	if len(out) != len(expecting) {
		tst.Errorf("Expected Filter(isEven) to cut the chain in half.  Got %d things instead.", len(out))
	}
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("Filter(isEven)[%d] != %d but %d", idx, expect, out[idx])
		}
	}

}

func TestFilterOnNonFunctionPredicate(tst *testing.T) {
	ch := Chain().Filter(0)
	if ch.Ok() {
		tst.Errorf("Expected Filter(0) to not be OK")
	}
	if ch != FILTER_FAILURE_NON_FUNCTION {
		tst.Errorf("Unexpected Filter(0) failure: %s", ch)
	}
}

func TestFilterOnBadFunctionArity(tst *testing.T) {
	ch := Chain().Filter(func() bool { return true })
	if ch.Ok() {
		tst.Errorf("Expected Filter(fn()bool) to not be OK")
	}
	if ch != FILTER_FAILURE_NON_MONADIC {
		tst.Errorf("Unexpected Filter(fn()bool) failure: %s", ch)
	}
}

func TestFilterOnNonReturningFunction(tst *testing.T) {
	ch := Chain().Filter(func(x int) {})
	if ch.Ok() {
		tst.Errorf("Expected Filter(fn(int)) to not be OK")
	}
	if ch != FILTER_FAILURE_WRONG_RETSIG {
		tst.Errorf("Unexpected Filter(fn(int)) failure: %s", ch)
	}
}

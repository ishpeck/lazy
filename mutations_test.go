package lazy

import (
	"testing"
)

func TestChainTakeOnBreak(tst *testing.T) {
	ch := Break("Deliberate failure").Take(4)
	if ch.Ok() {
		tst.Errorf("Taking anything from Break shouldn't make it OK")
	}
}

func TestChainTakeZeroFromEmpty(tst *testing.T) {
	ch := Chain().Take(0)
	if !ch.Ok() {
		tst.Errorf("Expected Take(0) on empty to work")
	}
}

func TestChainTakeZeroFromSingle(tst *testing.T) {
	ch := Chain(10).Take(0)
	if !ch.Ok() {
		tst.Errorf("Expected Take(0) on single to work")
	}
	out := ch.Every()
	if len(out) != 0 {
		tst.Errorf("Expected Take(0) to yield empty slice")
	}
}

func TestCHainTakeOneFromSingle(tst *testing.T) {
	ch := Chain(11).Take(1)
	if !ch.Ok() {
		tst.Errorf("Expecting Take(1) to be OK")
	}
	out := ch.Every()
	if len(out) != 1 {
		tst.Errorf("Expecting Take(1) to yield an element.")
		tst.FailNow()
	}
	if out[0] != 11 {
		tst.Errorf("Take(1) output is wrong.")
	}
}

func TestChainTakeFromThings(tst *testing.T) {
	ch := Chain(1, 2, 3, 4, 5, 6, 7).Take(4)
	if !ch.Ok() {
		tst.Errorf("Failed to Take(4) from 1-7")
		tst.FailNow()
	}
	out := ch.Every()
	expecting := []int{1, 2, 3, 4}
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("Take(4): %d != %d", out[idx], expect)
		}
	}
}

func TestBrokenChainTakeAfter(tst *testing.T) {
	ch := Break("Deliberate failure").After(3)
	if ch.Ok() {
		tst.Errorf("After() on broken chain shouldn't be OK")
	}
}

func TestAfterOnEmptyChain(tst *testing.T) {
	ch := Chain().After(1)
	if !ch.Ok() {
		tst.Errorf("empty.After(3): Unexpected failure: %s", ch)
	}
	out := ch.Every()
	if len(out) != 0 {
		tst.Errorf("empty.After(3) should yield empty but got %v.", out)
	}
}

func TestAfterOnShorterChain(tst *testing.T) {
	ch := Chain(1).After(2)
	if !ch.Ok() {
		tst.Errorf("[1].After(3): Unexpected failure: %s", ch)
	}
	out := ch.Every()
	if len(out) != 0 {
		tst.Errorf("[1].After(3) should yield empty but got %v.", out)
	}
}

func TestAfterOnSufficietnChain(tst *testing.T) {
	ch := Chain(1, 2, 3, 4, 5, 6).After(3)
	expecting := []int{4, 5, 6}
	if !ch.Ok() {
		tst.Errorf("many.After(3) Unexpected error: %s", ch)
	}
	tst.Log("About to resolve...")
	out := ch.Every()
	if len(out) != len(expecting) {
		tst.Errorf("many.After(3) Expected %d things but %d: %v", len(expecting), len(out), out)
		tst.FailNow()
	}
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("many.After(3)[%d] != %d but %d", idx, expect, out[idx])
		}
	}
}

func TestCollpaseOnBrokenChain(tst *testing.T) {
	ch := Break("Deliberate failure").Collapse()
	if ch.Ok() {
		tst.Errorf("Breaks shouldn't Collapse()")
	}
}

func TestCollapseOnChain(tst *testing.T) {
	ch := Chain(1, 2, 3).Collapse()
	if !ch.Ok() {
		tst.Errorf("Collapse() error unexpected: %s", ch)
	}
	out := ch.Every()
	if len(out) != 1 {
		tst.Errorf("Collapse() should make a chain have only 1 element.  Got %v", out)
		tst.FailNow()
	}
	lst := out[0]
	if lst == nil {
		tst.Errorf("The collapsed data is broken: %v", out)
		tst.FailNow()
	}
	if len(out[0].([]interface{})) != 3 {
		tst.Errorf("Expected the only element to be a slice of 3 things.  Got %v", out)
	}
}

func TestBrokenExpansion(tst *testing.T) {
	ch := Break("Deliberate failure")
	ex := ch.Expand()
	if ex != ch {
		tst.Errorf("Broken.Expand() should be identity")
	}
}

func TestExpansionBasic(tst *testing.T) {
	expecting := []int{1, 2, 3}
	ch := Chain(expecting)
	ex := ch.Expand(0)
	if !ex.Ok() {
		tst.Errorf("%s failed to expand: %s", ch, ex)
	}
	out := ex.Every()
	if len(out) != len(expecting) {
		tst.Errorf("Expected to have %d elements after expansion.  Got: %v", len(expecting), out)
		tst.FailNow()
	}
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("Expand failure: %v != %d (expected)", out[idx], expect)
		}
	}
}

func TestExpansionVersatility(tst *testing.T) {
	second := []int{1, 2, 3}
	fourth := []string{"foo", "bar", "bas"}
	ch := Chain("happy", second, 23, fourth)
	if !ch.Ok() {
		tst.Errorf("Expanding atomic failure: %s", ch)
	}
	attempt1 := ch.Expand(0)
	if !attempt1.Ok() {
		tst.Errorf("Expanding atomic things should yield that thing: %s", attempt1)
	}
	if attempt1.First() != "happy" {
		tst.Errorf("Expanding first thing should be happy")
	}
	attempt2 := ch.Expand(10000)
	nonexistent := attempt2.Every()
	if len(nonexistent) != 0 {
		tst.Errorf("There is no 10000th element yet somehow we got %v", nonexistent)
	}
	attempt3 := ch.Expand(1, 3)
	if !attempt3.Ok() {
		tst.Errorf("Expanding two things unexpectedly fails: %s", attempt3)
	}
	expecting := []interface{}{1, 2, 3, "foo", "bar", "bas"}
	out := attempt3.Every()
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("Multiexpande failure: %v != %v (expected)", out[idx], expect)
		}
	}

	attempt4 := ch.Expand(3, 1)
	if !attempt4.Ok() {
		tst.Errorf("Expanding two things in reverse unexpectedly fails: %s", attempt4)
	}
	expecting = []interface{}{"foo", "bar", "bas", 1, 2, 3}
	out = attempt4.Every()
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("Multiexpande failure: %v != %v (expected)", out[idx], expect)
		}
	}
	attempt5 := ch.Expand(1, 3, 1)
	if !attempt5.Ok() {
		tst.Errorf("Expanding two things in reverse unexpectedly fails: %s", attempt5)
	}
	expecting = []interface{}{1, 2, 3, "foo", "bar", "bas", 1, 2, 3}
	out = attempt5.Every()
	for idx, expect := range expecting {
		if out[idx] != expect {
			tst.Errorf("Multiexpande failure: %v != %v (expected)", out[idx], expect)
		}
	}

}

package lazy

func sequenceGetterNoop() interface{} {
	return nil
}

type Sequence interface {
	Advance() bool
	Copy() Sequence
	Cur() Getter
	Len() int
	Reset()
	Sub(...int) []Getter
	Valid() bool
}

type seqFetcher func() []Getter

type sequence struct {
	fetcher seqFetcher
	cache   []Getter
	cur     int
}

func (sq *sequence) Reset() {
	sq.cur = 0
}

func (sq *sequence) Len() int {
	return len(sq.fetch())
}

func (sq *sequence) Valid() bool {
	return sq.cur < sq.Len()
}

func (sq *sequence) fetch() []Getter {
	if sq.cache == nil {
		sq.cache = sq.fetcher()
	}
	return sq.cache
}

// NOTE: If your fetcher or getters happen to be stateful, copying can
// be dangerous.
func (sq *sequence) Copy() Sequence {
	if sq.cache != nil {
		return SpawnSeq(sq.cache...)
	}
	return SpawnUncachedSeq(sq.fetcher)
}

func (sq *sequence) Cur() Getter {
	if !sq.Valid() {
		return sequenceGetterNoop
	}
	ret := sq.fetch()[sq.cur]
	return ret
}

func (sq *sequence) Advance() bool {
	if !sq.Valid() {
		return false
	}
	sq.cur += 1
	return sq.Valid()
}

func (sq *sequence) pin(idx int) int {
	out := idx
	if out < 0 {
		out = 0
	}
	return out
}

func (sq *sequence) Sub(idxs ...int) []Getter {
	if sq.Len() < 1 {
		return sq.fetch()
	}
	if len(idxs) < 1 {
		return nil
	}
	if len(idxs) < 2 {
		return sq.fetch()[0:sq.pin(idxs[0])]
	}
	lb := sq.pin(idxs[0])
	ub := idxs[1]
	if lb >= sq.Len() {
		return []Getter{}
	}
	if ub > sq.Len() {
		ub = sq.Len()
	}
	return sq.fetch()[lb:ub]
}

func SpawnUncachedSeq(fetchFn seqFetcher) *sequence {
	return &sequence{
		fetchFn,
		nil,
		0,
	}
}

func SpawnSeq(args ...Getter) Sequence {
	return SpawnUncachedSeq(func() []Getter { return args })
}

func Seq(args ...interface{}) Sequence {
	var getters []Getter
	for _, arg := range args {
		getters = append(getters, Gen(arg))
	}
	return SpawnSeq(getters...)
}

# Lazy Chains

This is a library written in Go that attempts to introduce higher
order functions that lazily evaluate and do so concurrently wherever
possible.

# Installing

On Linux/Unix/Mac, open a terminal and run...

> go env GOPATH | xargs cd && cd src  
> git clone git@gitlab.com:ishpeck/lazy.git  
> cd lazy && go install  

Now you can use this library in your own projects.

On Windows systems, open a command prompt and run...

> cd go\src  
> git clone git@gitlab.com:ishpeck/lazy.git  
> cd lazy  
> go install  

# Example

Here's an example of some code...

```

package main

import (
	"fmt"
	"lazy"
	"strings"
)

func main() {
	words := lazy.Chain("hello,", "world").Map(func(inpt string) string {
		return strings.ToUpper(inpt)
	}).Every()
	fmt.Println(words...) // Output: HELLO, WORLD
	nums := lazy.Chain(1, 2, 3, 4, 5, 6, 7).Map(func(x int) int {
		return x * 2
	}).Filter(func(x int) bool {
		return x%6 == 0
	}).Every()
	fmt.Println(nums...) // Output: 6 12

	lazy.Chain(1, 2, 3, 4, 5, 6, 7, 8, 9).Map(func(x int) int {
		fmt.Printf("Now getting %d, ", x)
		return x
	}).Take(4).Every() // Output "Now getting X" where X is 1
			   // through 4, in unknown order because
			   // concurrency.
	fmt.Println()
}

```

# Chainable Methods

The Chainable interface provides methods which allow collections of
data to be processed and mutated lazily and concurrently wherever
possible.  Methods of a chainable object are as follows:

## ch.After(int) Chainable

Evaluates to a new chainable object with all elements found after
given index.

## ch.Apply(fn) Chainable

Passes all chained elements as arguments into fn() and evaluates down
to a new chainable object containing the values returned from fn()

## ch.Collapse() Chainable

Evaluates down to a new chainable object with a single element that is
a slice of interfaces containing original chained values.

## ch.Complaint() error

Evaluates down to an error that has occured previously in the chain or
`nil` if there were no errors.

## ch.Every() []interface{}

Resolves all deferred data lingering in the chain -- calling all
necessary functions in parallel to extract final values.  Returns a
slice of interfaces appropriate to the chain's behavior.

## ch.Expand(...int) Chainable

Evaluates down to a new chainable object whose elements are only the
values found at given indecies.  If the element pointed to by an index
happens to be a slice, its values are expanded in place to become
top-level elements in the chain.  Otherwise, the value is transcribed
as a new element.

If the index is not found in the chained elements, it will be skipped.

Example:

```
ch := Chain([]int{1, 2, 3}, 4, 5)
nu := ch.Expand(0)
fmt.Println(nu.Every()...) // Output: 1 2 3
db := ch.Expand(0, 1, 2)
fmt.Println(db.Every()...) // Output: 1 2 3 4 5
tp := ch.Expand(0, 0, 0, 1, 2)
fmt.Println(db.Every()...) // Output: 1 2 3 1 2 3 1 2 3 4 5
no := ch.Expand(10000) // No such element exists
fmt.Println("Got:", no.Every()) // Output: Got: []
sf := ch.Expand(2, 100, 200, 3)
fmt.Println(sf.Every()...) // Output: 4 5
```

## ch.Filter(fn) Chainable

Evaluates down to a new chainable object whose elements are only those
which, when passed into the predicate fn(), yield a true result.

Example:

```
isEven := func(x int) bool {
    return x%2 == 0
}
// Output: [2 4 6 8]
fmt.Println(Chain(1, 2, 3, 4, 5, 6, 7, 8).Filter(isEven).Every())
```

## ch.First() interface{}

Resolves the first element in the chain, calling all necessary
functions to extract the value of that one element.

## ch.Fold(fn) Chainable

Evaluates down to a new chainable object with a single element that
results from calling fn() as follows...

fn(fn(fn(el1, el2), el3), el4)

Example:

```
largest := func(x, y int) int {
	if x > y {
		return x
	}
	return y
}
// Output: 9
fmt.Println(lazy.Chain(0, 9, 1, 8, 2, 3).Fold(largest).First())
```

## ch.Guard(fn) Chainable

Eagerly resolves all data in the chain and passes that data into the
fn() which returns a nil if the chain should proceed or an error if
the chain should be broken.

If the error returned from fn() is non-nill, that error message
becomes the identifying string of the chain and all subsequent chain
steps will be skipped.

If every element in the chain passes through fn() without resulting in
an error, Guard's output is a new chainable object containing the
resolved values from the previous step in the chain.

## ch.Map(fn) Chainable

Evaluates down to a new chainable object whose elements are the output
of calling fn() with each of the elements in the chain.

## ch.Ok() bool

Returns true if there are no errors in the chain (yet).

## ch.Pleat(fn) Chainable

Evaluates down to a new chainable object whose elements are the output
of calling fn() as follows...

[fn(el1, el2), fn(el2, el3), ... fn(elN-1, elN)]

## ch.Resolve() ([]interface{}, error)

Short-hand for both ch.Every() and ch.Complaint().  Follows the
convention of Go programs to have errors as last returned values in
operations.

## ch.String() string

Returns a string that identifies the current chain step or the last
error message generated by the chain.

## ch.Take(int) Chainable

Evaluates down to a new chainable object whose elements are the first
N elements.

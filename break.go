package lazy

import "fmt"

type chBreak struct {
	err error
}

func (br *chBreak) After(idx int) Chainable {
	return br
}

func (br *chBreak) Apply(fn StencilFn) Chainable {
	return br
}

func (br *chBreak) Ok() bool {
	return false
}

func (br *chBreak) Pleat(fn StencilFn) Chainable {
	return br
}

func (br *chBreak) Resolve() ([]interface{}, error) {
	return nil, br.Complaint()
}

func (br *chBreak) Collapse() Chainable {
	return br
}

func (br *chBreak) Complaint() error {
	return br.err
}

func (br *chBreak) Every() []interface{} {
	return nil
}

func (br *chBreak) Expand(...int) Chainable {
	return br
}

func (br *chBreak) Filter(fn StencilFn) Chainable {
	return br
}

func (br *chBreak) First() interface{} {
	return nil
}

func (br *chBreak) Fold(fn StencilFn) Chainable {
	return br
}

func (br *chBreak) Guard(fn StencilFn) Chainable {
	return br
}

func (br *chBreak) Map(fn StencilFn) Chainable {
	return br
}

func (br *chBreak) String() string {
	return br.err.Error()
}

func (br *chBreak) Take(num int) Chainable {
	return br
}

func Break(formatStr string, args ...interface{}) Chainable {
	return &chBreak{
		fmt.Errorf(formatStr, args...),
	}
}
